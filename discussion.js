// CRUD
// Create , Read , Update, Delete

// Create
	// Allows us to create document under a collection or create a collection if it does not exist yet.

	/* Syntax

	db.collections.insertOne()
		- collections - stands for collection's name
		- allows us to insert or create

	*/

	db.users.insertOne(
		{
			"firstName" : "Tony",
			"lastName" : "Stark",
			"username" : "iAmIronMan",
			"email" : "iloveyou300@gmail.com",
			"password" : "starkindustries",
			"isAdmin" : true
		}
	)

/* db.collections.insertMany()
	insert multiple documents

*/

	db.users.insertMany([
		{
			"firstName" : "Pepper",
			"lastName" : "Potts",
			"username" : "rescueArmor",
			"email" : "pepper@gmail.com",
			"password" : "whereIsTonyAgain",
			"isAdmin" : false
		},
		{
			"firstName" : "Steve",
			"lastName" : "Roger",
			"username" : "theCaptain",
			"email" : "captAmerica@gmail.com",
			"password" : "iCanLiftMjolnirToo",
			"isAdmin" : false
		},
		{
			"firstName" : "Thor",
			"lastName" : "Odinson",
			"username" : "mightyThor",
			"email" : "ThorNotLoki@gmail.com",
			"password" : "iAmWorthyToo",
			"isAdmin" : false
		},
		{
			"firstName" : "Loki",
			"lastName" : "Odinson",
			"username" : "GodOfMischief",
			"email" : "loki@gmail.com",
			"password" : "iAmReallyLoki",
			"isAdmin" : false
		}

	])


	// Mini-Activity

db.courses.insertMany([
	{
		"name" : "JavaScript",
		"price" : 3500,
		"description" : "Learn JavaScript in a week",
		"isActive" : true
	},
	{
		"name" : "HTML",
		"price" : 1000,
		"description" : "Learn the Basic in 3 Days",
		"isActive" : true
	},
	{
		"name" : "CSS",
		"price" : 2000,
		"description" : "Make your website fancy",
		"isActive" : true
	}
])

// Read
	// allows us to retrieve data
		// it needs a query or filter

	/*
	snytax 

		- db.collections.find() 
			- allows us to retrieve all documents in the collection 

		-db.collections.findOne({})
			- allows us to retrieve or find document that matches to our criteria


	*/

	db.users.find();


	db.users.find({"isAdmin": false});
	

	/*
		db.collections.find({})
			-allows us to find the document that satisfy all criterias 

	*/
	db.users.find({"lastName" : "Odinson" , "firstName" : "Loki"});


	// Update

		/*
			Allows us to update documents
			this also use criteria or filter.
			$set operator
			REMINDER : Updates are permanent and can't be rollback


			Syntax
				-db.collections.updateOne({}, {$set:{}})
					-allows us to update one document that satisfies the criteria
		*/

		db.users.updateOne({"lastName" : "Potts"}, {$set: {"lastName" : "Stark"}})

		db.users.findOne({"firstName" : "Pepper" , "lastName" : "Stark"});

		/*
		db.collections.updateMany({criteria:value} , {$set: {"field to be updated" : "value of the new field"}});
			allows us to update all documents that satisfies the criteria

		*/

		db.users.updateMany({"lastName" : "Odinson"} , {$set: {"isAdmin" : true}});


		// Syntax 
			// db.collections.updateOne({}, {$set: {fieldtoBeUpdated : value}});
			// allows us to update the 1st item in the collection


		db.users.updateOne({}, {$set : {"email" : "starkindustries@mail.com"}});



// Mini activity

	db.courses.updateOne({"name" : "JavaScript"}, {$set : {"isActive" : false}});


	db.courses.updateMany({} , {$set : {"enrollees" : 10}});


	// Delete
		/*
		allows us to delete documents
		provides criteria or filter to specify which document to delete from the collection
		*/

		// Syntax
			// db.collections.deleteOne({criteria: value});

			db.users.deleteOne({"isAdmin" : false});

			// db.collections.deleteMany({criteria: value});

			db.users.deleteMany({"lastName" : "Odinson"});


			db.users.deleteOne({});

			db.users.deleteMany({});


// Mini-activity

