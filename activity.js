// Insert data

// Solution for number 3. 
// insert one room data

db.rooms.insertOne(

	{
		"name" : "single",
		"accomadates" : 2,
		"price" : 1000,
		"description" : "A simple room with all the basic necessities",
		"roomsAvailable" : 10,
		"isAvailable" : false

	}
)


// Solution for number 4
// Insert multiple room data

db.rooms.insertMany([

	{
		"name" : "double",
		"accomadates" : 3,
		"price" : 2000,
		"description" : "A room fit for a small family going on a vacation",
		"roomsAvailable" : 5,
		"isAvailable" : false

	},
	{
		"name" : "queen",
		"accomadates" : 4,
		"price" : 4000,
		"description" : "A room with a queen sized bed perfect for a simple gataway",
		"roomsAvailable" : 15,
		"isAvailable" : false

	}

])


// solutionn for number 5
// find method

db.rooms.findOne({"name" : "double"});


// solution for number 6
// updateOne method
db.rooms.updateOne({"name" : "queen"}, {$set : {"roomsAvailable" :0}});


// solution for number 7 
// deleteMany

db.rooms.deleteMany({"roomsAvailable" : 0})